from django.db import models
from django.utils import timezone


# Create your models here.

# This table will hold the names of the destinations the business travels to
class Destinations(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.name}'


# This table will hold the routes that lead to each destination
class Routes(models.Model):
    # the destination of the route
    destination = models.ForeignKey(Destinations, on_delete=models.PROTECT)
    start = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.start} to {self.destination.name}'


# This table will hold the dates when each route is travelled to a specific destination
class RoutesTravelDates(models.Model):
    # The route and to a specific destination
    route = models.ForeignKey(Routes, on_delete=models.PROTECT)
    # The date the route will be travelled
    travel_date = models.DateField(default=timezone.now)

    def __str__(self):
        return f'{self.route} route on {self.travel_date}'


# This table will hold the bookings made, the route that was booked and the phone number of the client
class Bookings(models.Model):
    route_date = models.ForeignKey(RoutesTravelDates, on_delete=models.PROTECT)
    phone_number = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.phone_number} {self.route_date} Booking'
