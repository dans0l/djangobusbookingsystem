from django.shortcuts import render, reverse

from django.views.generic import (ListView, CreateView, DeleteView, UpdateView)

from fleet.models import Destinations, Routes, RoutesTravelDates, Bookings


# Create your views here.

def home(request):
    return render(request, 'fleet/index.html')


# ------------------------------------------------------
# Destination Views
# ------------------------------------------------------

# The view to create a destination, it will inherit from the django built in CreateView
class DestinationsCreateView(CreateView):
    # it will act on the database table "Destinations"
    model = Destinations
    # It will require the following fields
    fields = ['name']
    # It will display the destination creation form generated by the view on the "destinations_detail.html"
    template_name = 'fleet/destinations_detail.html'

    # The url to be redirected to after a new destination is successfully added
    def get_success_url(self):
        # redirect user back to the page displaying a list of destinations
        return reverse('destinations-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Add New Destination'
        return context


class DestinationsListView(ListView):
    model = Destinations
    template_name = 'fleet/destination_list.html'
    context_object_name = 'destinations'


# The view to be used to delete a destination, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the Destinations
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class DestinationsDeleteView(DeleteView):
    model = Destinations
    success_url = '/'
    template_name = 'fleet/confirm_delete.html'

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete Destination'
        name = Destinations.objects.get(pk=self.kwargs.get('pk')).name
        context['message'] = f'Are you sure you want to delete the destination "{name}"'
        context['cancel_url'] = 'destinations-list'
        return context


# This view will be used to update the destination's details
class DestinationsUpdateView(UpdateView):
    model = Destinations
    fields = ['name']
    template_name = 'fleet/destinations_detail.html'

    def get_success_url(self):
        return reverse('destinations-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = 'Update Destination'
        return context


# ------------------------------------------------------
# Routes Views
# ------------------------------------------------------

# The view to create a destination's route, it will inherit from the django built in CreateView
class RoutesCreateView(CreateView):
    # it will act on the database table "Routes"
    model = Routes
    # It will require the following fields
    fields = ['start', 'destination']
    # It will display the form generated by the view on the "routes_detail.html"
    template_name = 'fleet/routes_detail.html'

    # The url to be redirected to after a destination has been linked to a route
    def get_success_url(self):
        # redirect user back to the page displaying a list of routes
        return reverse('routes-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Link Destination to a Route'
        return context


# The view to display the list of Destinations, it will inherit from the django built in ListView
# it will get its data from the Routes database table and display the data to the routes_list.html
# page and data will be accessed on the page using the "routes" variable object
class RoutesListView(ListView):
    model = Routes
    template_name = 'fleet/routes_list.html'
    context_object_name = 'routes'


# The view to be used to delete a Destinations, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the Routes
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class RoutesDeleteView(DeleteView):
    model = Routes
    template_name = 'fleet/confirm_delete.html'

    def get_success_url(self):
        return reverse('routes-list')

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = "Delete Destination's Route"
        route = Routes.objects.get(pk=self.kwargs.get('pk'))
        context[
            'message'] = f"Are you sure you want to delete {route.name} to {route.destination.name} route? "
        context['cancel_url'] = 'routes-list'
        return context


# This view will be used to update the destination's route details
class RoutesUpdateView(UpdateView):
    model = Routes
    fields = ['start', 'destination']
    template_name = 'fleet/routes_detail.html'

    def get_success_url(self):
        return reverse('routes-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = "Update Destination's Routes"
        return context


# ------------------------------------------------------
# RoutesTravelDates Views
# ------------------------------------------------------


# The view to create a destination's route travel dates, it will inherit from the django built in CreateView
class RoutesTravelDatesCreateView(CreateView):
    # it will act on the database table "RoutesTravelDates"
    model = RoutesTravelDates
    # It will require the following fields
    fields = ['route', 'travel_date']
    # It will display the user creation form generated by the view on the "route_dates_detail.html"
    template_name = 'fleet/route_dates_detail.html'

    # The url to be redirected to after a new destination's route travel date is successfully created
    def get_success_url(self):
        # redirect user back to the page displaying a list of destination's route travel dates
        return reverse('route-travel-dates-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Add New Route Travel Date'
        return context


# The view to display the list of destination's route travel dates, it will inherit from the django built in ListView
# it will get its data from the RoutesTravelDates database table and display the data to the route_dates_list.html
# page and data will be accessed on the page using the "route_travel_dates" variable object
class RoutesTravelDatesListView(ListView):
    model = RoutesTravelDates
    template_name = 'fleet/route_dates_list.html'
    context_object_name = 'route_travel_dates'


# The view to be used to delete a destination's route travel date, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the RoutesTravelDates
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class RoutesTravelDatesDeleteView(DeleteView):
    model = RoutesTravelDates
    template_name = 'fleet/confirm_delete.html'

    # The url to be redirected to after a new route's date is successfully created
    def get_success_url(self):
        # redirect user back to the page displaying a list of destination's route travel dates
        return reverse('route-travel-dates-list')

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = "Delete Destination's Route Travel Date"
        route_travel_date = RoutesTravelDates.objects.get(pk=self.kwargs.get('pk'))
        context[
            'message'] = f"Are you sure you want to delete {route_travel_date}'? "
        context['cancel_url'] = 'route-travel-dates-list'
        return context


# This view will be used to update the route's date of travel details
class RoutesTravelDatesUpdateView(UpdateView):
    model = RoutesTravelDates
    fields = ['route', 'travel_date']
    template_name = 'fleet/route_dates_detail.html'

    def get_success_url(self):
        return reverse('route-travel-dates-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = "Update Destination's Route Travel Date"
        return context


# ------------------------------------------------------
# Bookings Views
# ------------------------------------------------------


# The view to create a booking, it will inherit from the django built in CreateView
class BookingsCreateView(CreateView):
    # it will act on the database table "Bookings"
    model = Bookings
    # It will require the following fields
    fields = ['route_date', 'phone_number']
    # It will display the user creation form generated by the view on the "bookings_detail.html"
    template_name = 'fleet/bookings_detail.html'

    # The url to be redirected to after an appointment is successfully added
    def get_success_url(self):
        # redirect user back to the page displaying a list of bookings
        return reverse('bookings-list')

    # data is passed to the HTML/templates page in a context
    # override this method to get access to the context
    # and its data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # add/create a new context variable called "table_title"
        # this variable can then be accessed on the template
        context['table_title'] = 'Add New Booking'
        return context


# The view to display the list of Bookings, it will inherit from the django built in ListView
# it will get its data from the Bookings database table and display the data to the bookings_list.html
# page and data will be accessed on the page using the "bookings" variable object
class BookingsListView(ListView):
    model = Bookings
    template_name = 'fleet/bookings_list.html'
    context_object_name = 'bookings'


# The view to be used to delete a bookings, it will accept the "pk" as the variable holding the user_id
# it will inherit from the django built in DeleteView, it will perform the operation on the Bookings
# database table and will request a user to confirm the deletion operation on the confirm_delete.html
# It will finally state the url where a user is redirected after a successful deletion
class BookingsDeleteView(DeleteView):
    model = Bookings
    template_name = 'fleet/confirm_delete.html'

    def get_success_url(self):
        return reverse('bookings-list')

    # overrides this method so as to add custom data to the context object that will be pushed to the
    # HTML page displaying the data
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['title'] = 'Delete Booking'
        appointment = Bookings.objects.get(pk=self.kwargs.get('pk'))
        context['message'] = f'Are you sure you want to delete the "{appointment}"'
        context['cancel_url'] = 'bookings-list'
        return context


# This view will be used to update a booking's details
class BookingsUpdateView(UpdateView):
    model = Bookings
    fields = ['route_date', 'phone_number']
    template_name = 'fleet/bookings_detail.html'

    def get_success_url(self):
        return reverse('bookings-list')

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        context['table_title'] = 'Update Booking'
        return context
