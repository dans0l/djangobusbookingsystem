from django.urls import path

from fleet.views import (DestinationsListView, DestinationsCreateView, DestinationsUpdateView, DestinationsDeleteView,
                            RoutesListView, RoutesCreateView, RoutesUpdateView, RoutesDeleteView,
                            RoutesTravelDatesListView, RoutesTravelDatesCreateView, RoutesTravelDatesUpdateView, RoutesTravelDatesDeleteView,
                            BookingsListView, BookingsCreateView, BookingsUpdateView, BookingsDeleteView)

urlpatterns = [

    path('destinations', DestinationsListView.as_view(), name='destinations-list'),
    path('destinations/new/', DestinationsCreateView.as_view(), name='destinations-create'),
    path('destinations/<int:pk>/update/', DestinationsUpdateView.as_view(), name='destinations-update'),
    path('destinations/<int:pk>/delete/', DestinationsDeleteView.as_view(), name='destinations-delete'),

    path('routes', RoutesListView.as_view(), name='routes-list'),
    path('routes/new/', RoutesCreateView.as_view(), name='routes-create'),
    path('routes/<int:pk>/update/', RoutesUpdateView.as_view(), name='routes-update'),
    path('routes/<int:pk>/delete/', RoutesDeleteView.as_view(), name='routes-delete'),

    path('route_travel_dates', RoutesTravelDatesListView.as_view(), name='route-travel-dates-list'),
    path('route_travel_dates/new/', RoutesTravelDatesCreateView.as_view(), name='route-travel-dates-create'),
    path('route_travel_dates/<int:pk>/update/', RoutesTravelDatesUpdateView.as_view(), name='route-travel-dates-update'),
    path('route_travel_dates/<int:pk>/delete/', RoutesTravelDatesDeleteView.as_view(), name='route-travel-dates-delete'),

    path('bookings', BookingsListView.as_view(), name='bookings-list'),
    path('bookings/new/', BookingsCreateView.as_view(), name='bookings-create'),
    path('bookings/<int:pk>/update/', BookingsUpdateView.as_view(), name='bookings-update'),
    path('bookings/<int:pk>/delete/', BookingsDeleteView.as_view(), name='bookings-delete'),
]
