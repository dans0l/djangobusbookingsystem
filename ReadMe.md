# A Bus Booking System using Africa's Talking U.S.S.D. A.P.I.

## Quick Guide
Below are the steps on how to get the web app up and running

1.	Clone it:
```bash
    git clone https://bitbucket.org/dans0l/djangobusbookingsystem.git
```
2.	Cd into it:
```bash
    cd djangobusbookingsystem
```
3.	Create a venv
```bash
    python3 -m venv venv
```
4.	Activate venv:
```bash
    Mac/Linux: source venv/bin/activate
    Windows: venv\Scripts\activate
```
5.	Install the requirements
```bash
    pip install -r requirements.txt
```
6.	Create DB
```bash
    python manage.py makemigrations
```
7.	Apply DB Changes
```bash
    python manage.py migrate
```
8.	Create a Ngrok URL:
    - Download the ngrok software from their website [here](https://dashboard.ngrok.com/get-started/setup), on this page they you will be prompted to sign up to download the application, go ahead and do so.
    - Download and start/run the application
    - After running the Ngrok application you will see the ngrok terminal window
    - Go back to the Ngrok web page where you downloaded the application (the link is displayed above) and navigate to the section labeled “Connect your account”
    - Copy the ngrok code provided in the “Connect your account” section onto the Ngrok terminal
    - Inside the Ngrok terminal, type the command “ngrok http 8000”
    - A ngrok shell window will be displayed, with the name of the newly created ngrok http URL e.g. http://d9b253d6e13b.ngrok.io
    - Do not close or quit this ngrok shell
9.	Configure your Africa’s Talking U.S.S.D. A.P.I.:
    - Navigate to this [URL](https://account.africastalking.com/apps/sandbox/ussd/codes): you will be required to sign in or sing up if you don’t have an account with Africa’s Talking
    - While on that page click on the link labeled “create a channel” to create a USSD code
    - Enter a unique dummy channel e.g. 12345
    - Your U.S.S.D. Code is now e.g. *384*12345#
    - In the callback URL paste the ngrok http URL you created above, after appending “/api/ussd_callback/” at the end of the URL e.g. http://d9b253d6e13b.ngrok.io/api/ussd_callback/ this “/api/ussd_callback/” A.P.I. has already been created in the Django project.
    - Click on Create Channel
10.	Configure the Django Project to use the created ngrok url:
    - Open your Django project folder: “djangohospitalappointment”
    - Locate a folder with a similar name to your Django project folder, this folder is in the same directory as your “users” app folder
    - Inside this folder is a python file labeled “settings.py”
    - Open the “settings.py” file and locate the python variable ALLOWED_HOSTS, which is a python list located close to the start of the file
    - Inside the ALLOWED_HOSTS list enter the ngrok URL you created as well as the localhost URL, it should resemble something like this: ALLOWED_HOSTS = ['127.0.0.1','d9b253d6e13b.ngrok.io']
11.	Run the server:
    - python manage.py runserver
12.	Navigate to your [localhost](http://127.0.0.1:8000) site
13.	Follow the instructions on the home page to start using the site
