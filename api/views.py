from rest_framework.response import Response
from rest_framework.decorators import api_view
from fleet.models import Routes, RoutesTravelDates, Destinations, Bookings


# This method will be used to return either a list of destinations offered or a single destination offered
# by passing an index that will be used to get the specific destination from the list of destinations
# This index passed is the index a user selects, when prompted to choose the destination they want
def get_destination_object(index=None):
    destination = Destinations.objects.values_list('id', 'name')
    if index is None:
        # returns a QuerySet e.g. [(1, 'Kisumu'), (2, 'Nairobi'), (3, 'Mombasa')]
        return destination
    else:
        if 0 < index <= len(destination):
            # returns a single tuple with the destination id and name e.g.(1, 'Kisumu')
            return destination[index - 1]
        else:
            return None


# This method will be used to return either a list of routes that leads to a specific destination 
# or a single route that leads to the specified destination.
# By passing an index a user will be get the specific route that leads to the destination from the list
# of routes that leads to the specified destination.
# This index passed is the index a user selects, when prompted to choose the route they want to use
# This destination_id is the id of the destination they earlier selected
def get_destination_routes_object(destination_id, index=None):
    destination_routes = Routes.objects.filter(
        destination_id=destination_id).values_list('pk', 'start', 'destination__name')
    if index is None:
        # returns a QuerySet e.g. [(1, 'Meru', 'Nairobi'), (2, 'Kilifi', 'Nairobi'), (3, 'Voi', 'Kilifi')]
        return destination_routes
    else:
        if 0 < index <= len(destination_routes):
            # returns a single tuple with the destination route id, start of the route and destination e.g.(1, 'Meru', 'Nairobi')
            return destination_routes[index - 1]
        else:
            return None


# This method will be used to return either a list of dates the destination routes will be traveled
# or a single date the destination routes will be traveled.
# By passing an index a user will be get the specific date the destination route will be traveled from the list
# of dates the destination routes will be traveled.
# This index passed is the index a user selects, when prompted to choose the of date they want to travel the sepcified destination route
# This destination_routes_id is the id of the destination route they earlier selected.
def get_routes_travel_date_object(destination_routes_id, index=None):
    routes_travel_dates = RoutesTravelDates.objects.filter(
        route__id=destination_routes_id).values_list('pk', 'route', 'travel_date')
    if index is None:
        # returns a QuerySet e.g. [(1, '2021/10/13', 'Meru to Nairobi'), (2, '2021/10/15', 'Kilifi to Nairobi'), (3, '2021/12/13',
        # 'Voi to Kilifi')]
        return routes_travel_dates
    else:
        if len(routes_travel_dates) > 0:
            # returns a single tuple with the destination id and name e.g.(1, '2021/10/13', 'Meru to Nairobi')
            return routes_travel_dates[index - 1]
        else:
            return None


# A method used to generate destinations that will be displayed to a user
def get_destinations():
    # This is the index that will be displayed to the user for selection
    count = 0
    response = "CON Select Destination \n"
    for _, destination in get_destination_object():
        count += 1
        response += f"{count}. {destination} \n"
    return response


# The method used to generate a list of routes that lead to the selected destination
# You pass a destination_id that will be used to get routes leading to the specific destination
def get_destination_routes(destination_id):
    # This is the index that will be displayed to the user for selection
    count = 0
    destination_routes_object = get_destination_routes_object(destination_id=destination_id)
    
    if destination_routes_object is None:
        return "END No Route Available for the Destination \n"

    response = "CON Select Route \n"
    for _, start, destination__name in destination_routes_object:
        count += 1
        response += f"{count}. {start} to {destination__name} \n"

    if count == 0:
        response = "END No Route Available for the Destination \n"

    return response


# The method used to generate a list of dates the destination routes will be traveled.
# You pass a destination_routes_id that will be used to get dates the destination routes will be traveled, from the routes travel dates table.
def get_routes_travel_date(destination_routes_id):
    # This is the index that will be displayed to the user for selection
    count = 0
    routes_travel_date_object = get_routes_travel_date_object(destination_routes_id=destination_routes_id)

    if routes_travel_date_object is None:
        return "END No Dates Available for the Route \n"
        
    response = "CON Select Date to Book\n"
    for _, _, travel_date in routes_travel_date_object:
        count += 1
        response += f"{count}. {travel_date} \n"

    if count == 0:
        response = "END No Dates Available for the Route \n"
    return response


# The view that the U.S.S.D. will use to display data to the user
@api_view(['GET', 'POST'])
def ussd_callback(request):
    response = ""
    request = request.data
    phone_number = request.get("phoneNumber", None)
    text = request.get("text", "default")

    # The first call of the U.S.S.D. will send a blank text to the view, this will prompt us to first
    # display the destinations available

    if text == '':
        response = get_destinations()
    else:
        # All other calls after the first destination will not have an empty text, the text will state the index selected
        # during a particular stage or the index selected in the current stage after another index was selected from
        # a previous stage. e.g First Stage a user selects a destination, the text returned will be 1, indicating that
        # the selected the first destination that was displayed. The Second Stage a user selects a route,
        # the text returned will be 1*2, indicating that the selected the second route that was displayed after they
        # selected the first destination that was displayed

        # We are splitting the text so that we see if the user is selecting for the first time or they are selecting
        # again
        values = text.split("*")

        # If the user called the U.S.S.D. for the first time
        if len(values) == 1:
            # get the index of the destination that was selected
            selected_destination_index = int(values[0])
            # use this index to get the details of the destination that was actually selected or return None if a user
            # entered an index that was not displayed or does not exist.
            selected_destination = get_destination_object(selected_destination_index)
            selected_destination_id = -1
            if selected_destination is not None:
                # The destination returned will be a tuple with the id of the destination as the first element of the tuple
                selected_destination_id = selected_destination[0]

            # use the destination id get the routes that lead to the specified destination, or return an error response if
            # the destination selected has no routes leading to it.
            response = get_destination_routes(selected_destination_id)
        # If the user calls the U.S.S.D. for the second time and selects a destination route
        # The text split will have 2 values, one for the index of the selected destination and the
        # other for the index of the selected route
        elif len(values) == 2:
            # get the index of the destination that was selected
            selected_destination_index = int(values[0])
            # use this index to get the details of the destination that was actually selected or return None if a user
            # entered an index that was not displayed or a destination that  does not exist.
            selected_destination = get_destination_object(selected_destination_index)
            # The destination returned will be a tuple with the id of the destination as the first element of the tuple
            selected_destination_id = selected_destination[0]

            # get the index of the route that was selected
            selected_route_index = int(values[1])
            # use this index and destination id to get the details of the route that was actually selected or
            # return None if a user entered an index that was not displayed or a destination route index or id index that does not
            # exist.
            selected_destination_routes = get_destination_routes_object(destination_id=selected_destination_id,
                                                                 index=selected_route_index)

            selected_destination_routes_id = -1
            if selected_destination_routes is not None:
                # The route destination returned will be a tuple with the id of the route destination as the first
                # element of the tuple
                selected_destination_routes_id = selected_destination_routes[0]

            # using the route destination id to get the date the route will be traveled, or return an error
            # response if the route destination id selected has no travel date scheduled.
            response = get_routes_travel_date(selected_destination_routes_id)
        elif len(values) == 3:
            # get the index of the destination that was selected
            selected_destination_index = int(values[0])
            # use this index to get the details of the destination that was actually selected or return None if a user
            # entered an index that was not displayed or does not exist.
            selected_destination = get_destination_object(selected_destination_index)
            # The destination returned will be a tuple with the id of the destination as the first element of the tuple
            selected_destination_id = selected_destination[0]

            # get the index of the route that was selected
            selected_route_index = int(values[1])
            # use this index and destination id to get the details of the route that was actually selected or
            # return None if a user entered an index that was not displayed or a destination route index or id index that does not
            # exist.
            selected_destination_routes = get_destination_routes_object(destination_id=selected_destination_id,
                                                                 index=selected_route_index)
            # The route destination returned will be a tuple with the id of the route destination as the first
            # element of the tuple
            selected_destination_routes_id = selected_destination_routes[0]

            # get the index of the date the route will be traveled by the selected route for the selected destination
            selected_routes_travel_date_index = int(values[2])
            # use this index and route destination id to get the details of the date the route will be traveled or return None if a
            # user entered an index that was not displayed or a route date index that does not exist.
            selected_routes_travel_date = get_routes_travel_date_object(selected_destination_routes_id,
                                                                    index=selected_routes_travel_date_index)
            # The date the route will be traveled returned will be a tuple with the id of the date scheduled as the first
            # element of the tuple
            selected_routes_travel_date_id = selected_routes_travel_date[0]

            # use the date scheduled id to get the specified date scheduled object
            routes_travel_date = RoutesTravelDates.objects.get(pk=selected_routes_travel_date_id)

            # use the user's phone number to place the booking
            appointment = Bookings.objects.create(route_date=routes_travel_date, phone_number=phone_number)

            if appointment is None:
                response = "END Booking Failed"
            else:
                response = "END Booked Successfully"

    return Response(response, headers={"Content-Type": "text/plain"})
